# Kafka Example Application

The application includes consumer and producer modules:

- **Kafka-producer** is a web application that sends a message to an Apache Kafka instance using the HTTP POST method. The message can be received by a consumer. The content of the message is logged to the console
- **Kafka-consumer** is an application whose task is to receive and display the message received from the producer in the console

Before you run the application, you must install Apache Kafka instance on your computer. Instructions on how to do this can be found here: [https://kafka.apache.org/quickstart](https://kafka.apache.org/quickstart)

# Kafka Example Application in Docker container 

Another possibility to run the application is to run it as part of Docker container. For this we need: 
- Docker installed on your machine (instructions on how to do it can be found here: [https://docs.docker.com/get-docker/](https://docs.docker.com/get-docker/))

Additionally, changes to the files are necessary:

- **docker-compose.yml** - The localhost value in ADVERTISED_HOST should be replaced with the IP address of the machine where the application will be run

- *kafka-consumer* / **application.properties** - The localhost value in spring.kafka.bootstrap-servers should be replaced with the IP address of the machine where the application will be run

- *kafka-producer* / **application.properties** - The localhost value in spring.kafka.bootstrap-servers should be replaced with the IP address of the machine where the application will be run

How to start the application:

First, we build the SpringBoot project (the application does not contain unit tests) with the `mvn clean package -DskipTests=true` command

Then, in the project main directory, start building and starting the application in the container with the command: `docker-compose -f docker-compose.yml up --build`
