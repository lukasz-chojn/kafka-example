package kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;

import java.util.logging.Logger;

/**
 * The consumer startup class
 */
@SpringBootApplication
public class KafkaConsumerApplication {

    private static final Logger LOGGER = Logger.getLogger("KafkaConsumerApplication");

    private static final String KAFKA_TOPIC_NAME = "topic";
    private static final String KAFKA_GROUP_ID = "group-id";

    public static void main(String[] args) {
        LOGGER.info("Startująca aplikacja " + KafkaConsumerApplication.class.getSimpleName());
        SpringApplication.run(KafkaConsumerApplication.class, args);
    }

    @KafkaListener(topics = {KAFKA_TOPIC_NAME}, groupId = KAFKA_GROUP_ID)
    public void listenMessage(String message) {
        LOGGER.info(String.format("Otrzymana wiadomość o group-id: %s i treści: %s", KAFKA_GROUP_ID, message));
    }

}
