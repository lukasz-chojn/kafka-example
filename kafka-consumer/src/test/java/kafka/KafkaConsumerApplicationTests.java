package kafka;

import org.jsmart.zerocode.core.domain.Scenario;
import org.jsmart.zerocode.core.domain.TargetEnv;
import org.jsmart.zerocode.core.runner.ZeroCodeUnitRunner;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

@TargetEnv("kafka/kafka_test_server.properties")
@RunWith(ZeroCodeUnitRunner.class)
class KafkaConsumerApplicationTests {

    @Test
    @Scenario("kafka/test_kafka_consume.json")
    void testKafkaConsume() throws Exception {

    }
}
