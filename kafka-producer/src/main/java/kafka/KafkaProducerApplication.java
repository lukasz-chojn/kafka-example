package kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.Logger;

/**
 * Producer startup class
 */
@SpringBootApplication
public class KafkaProducerApplication {

    private static final Logger LOGGER = Logger.getLogger("KafkaProducerApplication");

    public static void main(String[] args) {
        LOGGER.info("Startująca aplikacja " + KafkaProducerApplication.class.getSimpleName());
        SpringApplication.run(KafkaProducerApplication.class, args);
    }

}
