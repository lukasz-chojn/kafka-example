package kafka.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

/**
 * The main controller of the producer application
 */
@RestController
@RequestMapping("/")
public class MainController {

    private final Logger LOGGER = Logger.getLogger("MainController");

    private KafkaTemplate<String, String> kafkaTemplate;
    @Value("${spring.kafka.template.default-topic}")
    private String kafkaTopicName;

    @Autowired
    public void setKafkaTemplate(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @PostMapping("/{message}")
    public void sendMessage(@PathVariable String message) {
        kafkaTemplate.send(kafkaTopicName, message);
        LOGGER.info(message);
    }
}
