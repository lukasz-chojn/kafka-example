package kafka.controller;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.MockitoAnnotations.openMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.testng.Assert.assertEquals;

public class MainControllerTest {

    private MockMvc mockMvc;
    @Mock
    private KafkaTemplate<String, String> kafkaTemplate;
    @InjectMocks
    private MainController mainController;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(mainController).build();
    }

    @Test
    public void testSendMessage() throws Exception {
        MockHttpServletRequestBuilder requestBuilder = post("/{message}", "test")
                .contentType(MediaType.TEXT_PLAIN_VALUE);

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertEquals(resultActions.andReturn().getResponse().getStatus(), 200);
    }
}